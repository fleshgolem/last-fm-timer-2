import {LFMTopTracksResponse, LFMTrack} from "@/model/lastfm";
import axios from 'axios';
import {Deserialize} from "cerialize";
import { flatMap } from 'lodash';
import ProgressStore from '@/stores/ProgressStore';

export enum LastFMPeriod {
    overall = "overall",
    sevenDay = "7day",
    oneMonth = "1month",
    threeMonths = "3month",
    sixMonths = "6month",
    twelveMonths = "12month"
}

class LastFMCacheKey {
    public user: string;
    public period: LastFMPeriod;

    constructor(user: string, period: LastFMPeriod) {
        this.user = user
        this.period = period
    }

    toString() {
        return `${this.user}:${this.period}`;
    }
}

type LastFMCacheEntry = { [id: number]: LFMTopTracksResponse };
type LastFMCache = { [id: string]: LastFMCacheEntry };

/* eslint-disable @typescript-eslint/camelcase */

export default class LastFMAPI {
    private static cache: LastFMCache = {};

    public static getTopTracks(user: string, page: number, period: LastFMPeriod): Promise<LFMTopTracksResponse> {
        const url = `https://ws.audioscrobbler.com/2.0/`;
        return axios.get(url, {
            params: {
                method: "user.gettoptracks",
                limit: "1000",
                user: user,
                api_key: process.env.LAST_FM_API_KEY,
                format: "json",
                period: period,
                page: page
            }
        }).then((r) => {
            return Deserialize(r.data, LFMTopTracksResponse);
        });
    }

    public static getTopTracksCached(user: string, page: number, period: LastFMPeriod): Promise<LFMTopTracksResponse> {
        const cacheKey = new LastFMCacheKey(user, period).toString();
        if (!this.cache[cacheKey]) {
            this.cache[cacheKey] = {};
        }
        if (this.cache[cacheKey][page]) {
            return Promise.resolve(this.cache[cacheKey][page]);
        }
        return this.getTopTracks(user, page, period).then(r => {
            this.cache[cacheKey][page] = r;
            return r;
        })
    }

    public static getAllPages(user: string, period: LastFMPeriod, retry = false): Promise<LFMTrack[]> {

        const isSuccessful = (p: PromiseSettledResult<LFMTopTracksResponse>): p is PromiseFulfilledResult<LFMTopTracksResponse> => {
            return p.status === "fulfilled";
        }
        const isFailure = (p: PromiseSettledResult<LFMTopTracksResponse>): p is PromiseRejectedResult => {
            return p.status === "rejected";
        }
        ProgressStore.state.loading = true;
        if (!retry) {
            ProgressStore.state.currentStep = "Load initial page";
        } else {
            ProgressStore.state.currentStep = "Retrying failed pages"
        }
        ProgressStore.state.progress = 0.0

        return this.getTopTracksCached(user, 1, period).then(r => {
            const pageCount = r.toptracks.attributes.totalPagesNumber;

            if (!retry) {
                ProgressStore.state.currentStep = `Load ${pageCount - 1} more pages of tracks`;
            }

            const promises: Promise<LFMTopTracksResponse>[] = [];
            let successes = 1.0;
            ProgressStore.state.progress = successes / pageCount;

            for (let i = 2; i <= pageCount; i++) {
                promises.push(this.getTopTracksCached(user, i, period).then(r => {
                    successes += 1;
                    ProgressStore.state.progress = successes / pageCount;
                    return r;
                }));
            }
            return Promise.allSettled(promises).then(results => {
                const successes: PromiseFulfilledResult<LFMTopTracksResponse>[] = [];
                const failures: PromiseRejectedResult[] = [];
                for (const r of results) {
                    if (isSuccessful(r)) {
                        successes.push(r);
                    } else if (isFailure(r)) {
                        failures.push(r);
                    }
                }
                if (failures.length > 0) {
                    throw new Error("At least one failure")
                }
                return successes.map(r => { return r.value.toptracks.tracks });
            }).then(arrays => {
                ProgressStore.state.loading = false;
                return [...r.toptracks.tracks, ...flatMap(arrays)];
            }).catch(() => {
                // just try, try, try again (will only request uncached pages this time)
                return this.getAllPages(user, period);
            });
        })
    }

}
/* eslint-enable @typescript-eslint/camelcase */
