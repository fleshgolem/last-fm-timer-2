interface ProgressStore {
    state: {
        progress: number;
        loading: boolean;
        currentStep: string | null;
    };
}

const store: ProgressStore = {
    state: {
        progress: 0.0,
        loading: false,
        currentStep: null
    }
}
export default store;
