import { autoserialize, autoserializeAs } from "cerialize";

export enum SizeClass {
  small = 'small',
  medium = 'medium',
  large = 'large',
  extraLarge = 'extraLarge',
}

export class LFMTrackImage {
  @autoserializeAs('#text') public text: string;
  @autoserializeAs(SizeClass) public size: string;

  constructor(size: SizeClass, text: string) {
    this.size = size;
    this.text = text;
  }
}

export class LFMArtist {
  @autoserialize public mbid: string;
  @autoserialize public name: string
  @autoserialize public url: string;

  constructor(mbid: string, name: string, url: string) {
    this.mbid = mbid;
    this.name = name;
    this.url = url;
  }
}

export class LFMTrack {
  @autoserialize public duration: string;
  @autoserialize public playcount: string;
  @autoserialize public mbid: string;
  @autoserialize public name: string
  @autoserialize public url: string;
  @autoserializeAs(LFMArtist, 'artist') public artist: LFMArtist;
  @autoserializeAs(LFMTrackImage, 'image') public images: LFMTrackImage[];

  get durationNumber(): number {
    return parseInt(this.duration)
  }

  get playcountNumber(): number {
    return parseInt(this.playcount)
  }

  constructor(duration: string, playcount: string, mbid: string, name: string, url: string, artist: LFMArtist) {
    this.duration = duration;
    this.playcount = playcount;
    this.mbid = mbid;
    this.name = name;
    this.url = url;
    this.images = [];
    this.artist = artist;
  }
}

export class LFMAttributes {
  @autoserialize public page: string;
  @autoserialize public total: string;
  @autoserialize public user: string;
  @autoserialize public perPage: string;
  @autoserialize public totalPages: string;

  get pageNumber() {
    return parseInt(this.page);
  }
  get totalNumber() {
    return parseInt(this.total);
  }
  get perPageNumber() {
    return parseInt(this.perPage);
  }
  get totalPagesNumber() {
    return parseInt(this.totalPages);
  }

  constructor(
    page: string,
    total: string,
    user: string,
    perPage: string,
    totalPages: string
  ) {
    this.page = page;
    this.total = total;
    this.user = user;
    this.perPage = perPage;
    this.totalPages = totalPages;
  }
}

class LFMTopTracks {
  @autoserializeAs(LFMAttributes, "@attr") public attributes: LFMAttributes;
  @autoserializeAs(LFMTrack, "track") public tracks: LFMTrack[];
  constructor(attributes: LFMAttributes) {
    this.attributes = attributes;
    this.tracks = [];
  }
}

export class LFMTopTracksResponse {
  @autoserializeAs(LFMTopTracks) public toptracks: LFMTopTracks;

  constructor(toptracks: LFMTopTracks) {
    this.toptracks = toptracks;
  }
}
