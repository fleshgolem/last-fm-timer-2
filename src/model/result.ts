import Timespan from "@/helper/timespan";

export default class LastFMResult {
    public rank = 0
    public displayName: string
    public playtime: number
    public playcount: number

    constructor(displayName: string, playtime: number, playcount: number) {
        this.displayName = displayName;
        this.playtime = playtime;
        this.playcount = playcount;
    }

    public get formattedPlaytime(): string {
        const timespan = new Timespan(this.playtime)
        return timespan.toString()
    }

    public get average(): string {
        const avg = Math.floor(this.playtime / this.playcount);
        const timespan = new Timespan(avg)
        return timespan.toString()
    }




}
