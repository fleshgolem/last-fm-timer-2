export default class Timespan {
    days: number
    hours: number
    minutes: number
    seconds: number

    constructor(seconds: number) {
        this.days = Math.floor(seconds / (60 * 60 * 24))
        let rest = seconds - (this.days * (60 * 60 * 24))
        this.hours = Math.floor(rest / (60 * 60))
        rest -= this.hours * 60 * 60
        this.minutes = Math.floor(rest / 60)
        rest -= this.minutes * 60
        this.seconds = rest
    }

    toString(): string {
        if (this.days) {
            return `${this.pad(this.days, 2)}:${this.pad(this.hours, 2)}:${this.pad(this.minutes, 2)}:${this.pad(this.seconds, 2)}`
        }
        if (this.hours) {
            return `${this.pad(this.hours, 2)}:${this.pad(this.minutes, 2)}:${this.pad(this.seconds, 2)}`
        }
        return `${this.pad(this.minutes, 2)}:${this.pad(this.seconds, 2)}`
    }

    private pad(num: number | string, size: number) {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }
}
