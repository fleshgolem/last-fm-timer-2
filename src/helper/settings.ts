export default class Settings {
    static get darkMode() {
        return (localStorage.getItem('darkMode') ?? "1") === "1";
    }

    static set darkMode(val: boolean) {
        localStorage.setItem('darkMode', val ? "1" : "0");
    }
}
