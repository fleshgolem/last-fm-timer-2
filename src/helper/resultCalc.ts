import {LFMTrack} from "@/model/lastfm";
import Mode from "@/model/mode";
import LastFMResult from "@/model/result";
import { sortBy } from 'lodash';

interface GroupKey {
    id: string | null;
    display: string;
}

export default {
    groupByTrack(input: LFMTrack): GroupKey {
        return {
            id: `${input.name}${input.mbid}`,
            display: `${input.artist.name} - ${input.name}`
        }
    },

    groupByArtist(input: LFMTrack): GroupKey {
        return {
            id: `${input.artist.name}${input.artist.mbid}`,
            display: input.artist.name
        }
    },

    groupingFunction(mode: Mode): (input: LFMTrack) => GroupKey {
        switch (mode) {
            case Mode.artist:
                return this.groupByArtist;
            case Mode.track:
                return this.groupByTrack;
        }
    },

    calculateResults(input: LFMTrack[], mode: Mode): LastFMResult[] {
        const groups: { [id: string]: LastFMResult } = {};
        for (const t of input) {
            const key = this.groupingFunction(mode)(t);
            if (!key.id) {
                continue;
            }
            if (!groups[key.id]) {
                groups[key.id] = new LastFMResult(key.display, 0, 0);
            }
            groups[key.id].playtime += t.durationNumber * t.playcountNumber;
            groups[key.id].playcount += t.playcountNumber;
        }
        return sortBy(Object.values(groups), 'playtime')
            .reverse()
            .map((r, idx) => {
                // attach rank
                r.rank = idx + 1;
                return r;
            });
    }
}
