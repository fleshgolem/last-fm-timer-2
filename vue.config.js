webpack = require('webpack');

module.exports = {
  configureWebpack: {
    plugins: [
        new webpack.EnvironmentPlugin(['LAST_FM_API_KEY'])
    ]
  },
  transpileDependencies: ["vuetify"]
};
