# last-fm-timer-2

## Project setup
```
yarn install
```

### IMPORTANT!

Before running or building this app you need to set the environment variable `LAST_FM_API_KEY` to a valid last.fm api key

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
